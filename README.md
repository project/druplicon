CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Druplicon is a simple module that allow you to customize the default Druplicon
in the Admin Toolbar that comes from the module [admin_toolbar](https://www.drupal.org/project/admin_toolbar).


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for details.


CONFIGURATION
-------------

* To upload a new druplicon go to Administration » Configuration »
  User interface » Druplicon (/admin/config/druplicon/settings).


MAINTAINERS
-----------

Current maintainers:

 * [Azz-eddine BERRAMOU (berramou)](https://www.drupal.org/u/berramou)
