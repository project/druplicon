(function ($, Drupal) {
  Drupal.behaviors.druplicon = {
    attach: function (context, settings) {
      if (settings.druplicon) {
        // Add druplicon for D10.
        if ($('.toolbar-icon-10 .toolbar-icon-admin-toolbar-tools-help', context).length) {
          $(
            `<style>
            .toolbar-icon-10 .toolbar-icon-admin-toolbar-tools-help:before {
               background-image: url("${settings.druplicon}") !important;
             }
         </style>`,
          ).appendTo('head');
        }
        // Add druplicon for D9.
        if ($('.toolbar-icon-9 .toolbar-icon-admin-toolbar-tools-help', context).length) {
          $(
            `<style>
            .toolbar-icon-9 .toolbar-icon-admin-toolbar-tools-help:before {
               background-image: url("${settings.druplicon}") !important;
             }
         </style>`,
          ).appendTo('head');
        }
      }
    },
  };
})(jQuery, Drupal);
