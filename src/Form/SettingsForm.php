<?php

namespace Drupal\druplicon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a druplicon settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'druplicon_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'druplicon.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('druplicon.settings');

    $form['druplicon'] = [
      '#type'              => 'managed_file',
      '#title'             => $this->t('Upload druplicon image'),
      '#description'       => $this->t('This field to upload your new druplicon image.'),
      '#upload_location'   => 'public://druplicon/',
      '#default_value'     => $config->get('druplicon_fid') ? [$config->get('druplicon_fid')] : [],
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpeg jpg jpe webp svg'],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the uploaded file ID to configuration.
    $config = $this->config('druplicon.settings');
    if (isset($form_state->getValue('druplicon')[0]) && $fid = (int) $form_state->getValue('druplicon')[0]) {
      $config->set('druplicon_fid', $fid)
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
